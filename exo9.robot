*** Settings ***
Resource	exo9.resource
Library  SeleniumLibrary

*** Variables ***
${url}  https://katalon-demo-cura.herokuapp.com/
${browser}  firefox

*** Keywords ***
Test Setup
    Open Browser    ${url}    ${browser}
    Maximize Browser Window

Test Teardown
    Sleep    5
    Close browser 

*** Test Cases ***
test medicare
	[Tags]    tokyo
	
	[Setup]	Test Setup

	Given Je suis connecte a la page de prise de rendez-vous    John Doe  ThisIsNotAPassword
	When Je renseigne les informations obligatoires    ${jdd1.facility}
	And Je choisis HealthcareProgramme    ${jdd1.hcp}
	And Je clique sur book appointement  
	Then Le rendez vous est confirmé et le HealthcareProgramme est affiche    ${jdd1.hcpres}    ${jdd1.facility}

	[Teardown]	Test Teardown

test medicaid
	[Setup]	Test Setup

    Given Je suis connecte a la page de prise de rendez-vous    John Doe  ThisIsNotAPassword
	When Je renseigne les informations obligatoires    ${jdd2.facility}
	And Je choisis HealthcareProgramme    ${jdd2.hcp}
	And Je clique sur book appointement  
	Then Le rendez vous est confirmé et le HealthcareProgramme est affiche    ${jdd2.hcpres}    ${jdd2.facility}

	[Teardown]	Test Teardown

test none
	[Setup]	Test Setup

    Given Je suis connecte a la page de prise de rendez-vous    John Doe  ThisIsNotAPassword
	When Je renseigne les informations obligatoires    ${jdd3.facility}
	And Je choisis HealthcareProgramme    ${jdd3.hcp}
	And Je clique sur book appointement  
	Then Le rendez vous est confirmé et le HealthcareProgramme est affiche    ${jdd3.hcpres}    ${jdd3.facility}

	[Teardown]	Test Teardown